package com.company;

/**
  Created by Chinildo on 30/03/2017.
 O bairro Boa Vista é formado por um conjunto de quadras de mesmo tamanho, dispostas em um
 quadriculado de N por M quadras, com ruas no sentido Norte-Sul e no sentido Leste-Oeste. Assim,
 o comprimento de cada rua no sentido Norte-Sul é igual a N quadras e o comprimento de cada
 rua no sentido Leste-Oeste é igual a M quadras. A atual administração da prefeitura decidiu que
 é necessário escolher uma rua do bairro, no sentido Norte-Sul, para ser alargada. Para isso, será
 necessário desapropriar todas as quadras de um dos lados da rua escolhida.
 As quadras têm construções diferentes, de forma que cada quadra tem um valor diferente no mercado.
 Para desapropriar as quadras, a prefeitura tem que pagar o valor do mercado aos proprietários.
 A figura abaixo mostra um exemplo dos valores das quadras, em milhões de reais, onde N = 3 e
 M = 4.
 Sua tarefa é escrever um programa que, dados os valores das quadras, em milhões de reais, determine
 qual o menor valor que a prefeitura terá que desembolsar.

 */
import java.util.*;

public class avenida {

    public static void main(String[] args )    {
        Scanner input = new Scanner(System.in);
        int N = input.nextInt(), M  = input.nextInt();
        int [] [] matriz = new int[N][M];
        for(int i=0 ; i < N ; i++){
            for(int j = 0; j < M ; j ++) {
                matriz[i][j] = input.nextInt();
            }
        }
        int menor = Integer.MAX_VALUE;
        for (int i=0; i <M; i++){
            int soma =0;
            for (int j= 0; j < N; j++){
                soma = soma + matriz[j][i];
            }
            if(soma<menor){
                menor = soma;
            }
        }
        System.out.println(menor);
    }




}
