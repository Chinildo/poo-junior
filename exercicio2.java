package com.company;

import java.util.*;

public class Main {

    public static int [] vetorAleat(int qtd, boolean valorlogico){
        Random aleatorio = new Random();
        int [] vetor = new int[qtd];
        for (int i = 0; i < vetor.length; i++) {
            vetor[i] = aleatorio.nextInt( 100)*2;
            if(!valorlogico) {
                vetor[i] = vetor[i]+1;
            }
        }
        return vetor;
    }
    public static int [] vetorFatorial(int [] vetor){
        int [] vetorFatorado = new int[vetor.length];
        for (int i = 0; i < vetor.length; i++){
            vetorFatorado[i] = fatorar(vetor[i]);
        }
        return vetorFatorado;
    }
    public static int fatorar (int numero){
        int Nfatorial = 1;
        while(numero!=0){
            Nfatorial = Nfatorial*numero;
            numero--;
        }
        return Nfatorial;
    }
    public static void printVet(int[] vetor) {
        for (int i = 0; i < vetor.length; i++){
            System.out.println(vetor[i]);
        }
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Random aleatorio = new Random();
        System.out.println("Informe a qtd");
        int qtdVet = input.nextInt();
        int [] vetorA = new int []{};
        int [] vetor = new int [4];
        boolean valorlogico = false;
        System.out.println("informe o valor logico");
        valorlogico = input.nextBoolean();
        vetorA=vetorAleat(qtdVet, valorlogico);
        printVet(vetorA);
    }


}
