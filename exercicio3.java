package com.company;

import java.util.Scanner
        ;
public class Main {
    public static boolean ConferirSoma(double [] vetor, double resultado){
        boolean VlrLogico = false;
        double soma = 0;
        for (int i = 0; i<vetor.length; i++){
            soma = soma+vetor[i];
        }
        if(soma == resultado){
            VlrLogico = true;
        }
        return VlrLogico;
    }


    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double [] vetorA = new double[5];
        boolean soma = false;
        double resultado = 0;
        System.out.println("Preencha o vetor: ");
        for (int i = 0; i<vetorA.length; i++){
            vetorA[i] = input.nextDouble();
        }
        System.out.println("Insira o resultado");
        resultado = input.nextDouble();
        soma = ConferirSoma(vetorA,resultado);
        System.out.println(soma);
    }
}
