package com.company;

import java.util.Scanner;

public class Main {

    public static float fatorar (int numero){
        if(numero<=1){
            return 1;
        }
        return fatorar(numero-1)*numero;
    }
    public static double equacao(int N){
        if (N == 0) {
            return 0;
        }
        return(1.0/fatorar(N))+equacao(N-1);
    }
    public static void main(String[] args) {
        System.out.println(equacao(2));

    }
}