package com.company;

import java.util.*;

public class Main {

     public static void preencherVetor(int vetor[]){
        Scanner input = new Scanner(System.in);
        for (int i = 0; i < vetor.length; i++) {
            System.out.print("Informe as idades: ");
            vetor[i] = input.nextInt();
        }
    }
     public static double mediaVet(int vetor[]){
         double media = 0;
         for (int i = 0; i<vetor.length; i++){
             media = media + vetor[i];
         }
            media=media/vetor.length;
         return media;
     }
     public static int contador(int vetor[], double media){
         int cont = 0;
         for (int i = 0; i<vetor.length; i++){
             if (vetor[i]>media){
             cont++;
             }
            }
         return cont;
    }

     public static void main(String[] args) {
         Scanner input = new Scanner(System.in);
         int [] vetorMasc = new int [5];
         int [] vetorFem = new int [5];
         int contMasc = 0, contFem = 0;
         double mediaFem = 0, mediaMasc = 0;
         System.out.println("Preencha o vetor masculino");
         preencherVetor(vetorMasc);
         mediaMasc = mediaVet(vetorMasc);
         System.out.println("Preencha o vetor Feminino");
         preencherVetor(vetorFem);
         mediaFem = mediaVet(vetorFem);
         contMasc = contador(vetorMasc, mediaFem);
         contFem = contador(vetorFem, mediaMasc);
         System.out.println("Quantidade de mulheres acima da m�dia dos homens: " +contFem);
         System.out.println("Quantidade de homens acima da m�dia das mulheres: " +contMasc);
     }
}
