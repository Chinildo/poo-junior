package com.company;

import java.util.Scanner;

public class Main{
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Informe um número");
        int numero = input.nextInt();
        int unidadeMil= 0, unidadeCent=0, unidadeDez=0, unidade=0;
        while(numero<0 || numero >100000){
            System.out.println("Número invalido, tente outro");
            numero = input.nextInt();
        }
        String [] VetorUnid = {""," um", " dois", " três", " quatro", " cinco", " seis", " sete", " oito", " nove", " dez", " onze"
                , " doze", " treze", " quatorze", " quinze"," dezesseis", " dezessete", " dezoito", " dezenove"};
        String [] VetorDez = {"",""," vinte"," trinta"," quarenta"," cinquenta", " sessente", " setenta", " oitenta", " noventa"};
        String [] VetorCent = {""," cento"," duzentos", " trezentos", " quatrocentos", " quinhentos", " seiscentos", " setecentos", " oitocentos", " novecentos"};
        String Mil = "";
        unidadeMil = numero/1000;
        unidadeCent = numero%1000/100;
        unidadeDez = numero%100/10;
        unidade = numero%10;
        if(unidadeDez <2) {
            unidade = (unidadeDez*10)+unidade;
        }
        if(unidadeMil>0){
            Mil =" mil";
        }
        if(unidadeDez>2){
            VetorDez[unidadeDez] = " e" +VetorDez[unidadeDez];
        }
        if(unidade>0){
            VetorUnid[unidade] = " e" +VetorUnid[unidade];
        }
        System.out.println(VetorUnid[unidadeMil]+Mil+VetorCent[unidadeCent]+VetorDez[unidadeDez]+VetorUnid[unidade]);
    }
}
