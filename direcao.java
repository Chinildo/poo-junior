package com.company;

import java.util.Scanner;

/**
 * Created by Chinildo on 30/03/2017.
 Seu amigo está perdido no deserto e precisa encontrar um oásis. Você sabe que seu amigo está, no
        momento, virado para um dos pontos cardeais (norte, sul, leste, oeste). Você também sabe que o
        oásis mais próximo está em uma dessas quatro direções. Dadas essas duas informações, você deve
        dizer qual o menor ângulo, em graus, que seu amigo deverá virar para ir na direção do oásis mais
        próximo.
 */
public class direcao {public static void main(String[] args) {
    Scanner input = new Scanner(System.in);
    String A = input.nextLine();
    String B = input.nextLine();
    if(A.equalsIgnoreCase(B)){
        System.out.println("0");
    }
    else if((A.equalsIgnoreCase("leste")&& B.equalsIgnoreCase("oeste"))||(B.equalsIgnoreCase("leste")&&A.equalsIgnoreCase("oeste"))){
        System.out.println("180");
    }
    else if((A.equalsIgnoreCase("norte")&& B.equalsIgnoreCase("sul"))||(B.equalsIgnoreCase("norte")&&A.equalsIgnoreCase("sul"))){
        System.out.println("180");
    }
    else if((A.equalsIgnoreCase("norte")&& B.equalsIgnoreCase("leste"))||(A.equalsIgnoreCase("leste")&&B.equalsIgnoreCase("norte"))){
        System.out.println("90");
    }
    else if((A.equalsIgnoreCase("norte")&& B.equalsIgnoreCase("oeste"))||(A.equalsIgnoreCase("oeste")&&B.equalsIgnoreCase("norte"))){
        System.out.println("90");
    }
    else if((A.equalsIgnoreCase("sul")&& B.equalsIgnoreCase("oeste"))||(A.equalsIgnoreCase("sul")&&B.equalsIgnoreCase("oeste"))){
        System.out.println("90");
    }
    else if((A.equalsIgnoreCase("sul")&& B.equalsIgnoreCase("leste"))||(A.equalsIgnoreCase("leste")&&B.equalsIgnoreCase("sul"))){
        System.out.println("90");
    }


}
}
