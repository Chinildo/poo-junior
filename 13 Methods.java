package com.company;

import java.util.*;

public class Main {

    public static void preencherVetor(int vetor[]){
        Scanner input = new Scanner(System.in);
        for (int i = 0; i < vetor.length; i++) {
            System.out.print("Informe as idades: ");
            vetor[i] = input.nextInt();
        }
    }
    public static double mediaVet(int vetor[]){
        double media = 0;
        for (int i = 0; i<vetor.length; i++){
            media = media + vetor[i];
        }
        media=media/vetor.length;
        return media;
    }
    public static int contador(int vetor[], double media){
        int cont = 0;
        for (int i = 0; i<vetor.length; i++){
            if (vetor[i]>media){
                cont++;
            }
        }
        return cont;
    }
    public static int [] vetorAleat(int qtd, boolean valorlogico){
        Random aleatorio = new Random();
        int [] vetor = new int[qtd];
        for (int i = 0; i < vetor.length; i++) {
            vetor[i] = aleatorio.nextInt( 100)*2;
            if(!valorlogico) {
                vetor[i] = vetor[i]+1;
            }
        }
        return vetor;
    }
    public static int [] vetorFatorial(int [] vetor){
        int [] vetorFatorado = new int[vetor.length];
        for (int i = 0; i < vetor.length; i++){
            vetorFatorado[i] = fatorar(vetor[i]);
        }
        return vetorFatorado;
    }
    public static int fatorar (int numero){
        if(numero<=1){
           return 1;
        }
        return fatorar(numero-1)*numero;
    }
    public static int somaNate1 (int numero){
        if(numero==0){
            return 0;
        }
        return somaNate1(numero-1)+numero;
    }
    public static void printVet(int[] vetor) {
        for (int i = 0; i < vetor.length; i++){
            System.out.println(vetor[i]);
        }
    }
    public static boolean ConferirSoma(double [] vetor, double resultado) {
        boolean VlrLogico = false;
        double soma = 0;
        for (int i = 0; i < vetor.length; i++) {
            soma = soma + vetor[i];
        }
        if (soma == resultado) {
            VlrLogico = true;
        }
        return VlrLogico;
    }
    public static int ModuloN(int N){
        int modulo = N;
        if(modulo<0){
            modulo = modulo*(-1);
        }
        return modulo;
    }
    public static int arredondar(double N){
        int arredondado = (int) N;
        return arredondado;
    }
    public static float Nmaior(float N1, float N2){
        float maior = N1;
        if(N2>N1){
            maior = N2;
        }
        return maior;
    }
    public static String NumeroporExtenso (int numero){
        Scanner input = new Scanner(System.in);
        int unidadeMil= 0, unidadeCent=0, unidadeDez=0, unidade=0;
        String [] VetorUnid = {""," um", " dois", " três", " quatro", " cinco", " seis", " sete", " oito", " nove", " dez", " onze"
                , " doze", " treze", " quatorze", " quinze"," dezesseis", " dezessete", " dezoito", " dezenove"};
        String [] VetorDez = {"",""," vinte"," trinta"," quarenta"," cinquenta", " sessente", " setenta", " oitenta", " noventa"};
        String [] VetorCent = {""," cento"," duzentos", " trezentos", " quatrocentos", " quinhentos", " seiscentos", " setecentos", " oitocentos", " novecentos"};
        String Mil = "";
            unidadeMil = numero / 1000;
            unidadeCent = numero % 1000 / 100;
            unidadeDez = numero % 100 / 10;
            unidade = numero % 10;
            if (unidadeDez < 2) {
                unidade = (unidadeDez * 10) + unidade;
            }
            if (unidadeMil > 0) {
                Mil = " mil";
            }
            if (unidadeDez > 2) {
                VetorDez[unidadeDez] = " e" + VetorDez[unidadeDez];
            }
            if (unidade > 0 && unidadeDez > 0) {
                VetorUnid[unidade] = " e" + VetorUnid[unidade];
            }
        return (VetorUnid[unidadeMil] + Mil + VetorCent[unidadeCent] + VetorDez[unidadeDez] + VetorUnid[unidade]);
    }


    public static void main(String[] args) {

    }
}
