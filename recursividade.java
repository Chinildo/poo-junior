package com.company;

import java.util.Scanner
        ;
public class Main {
    public static int SomaRecu(int N){
        int soma = 0;
        if(N<=0) {
           return N;
        }
        soma = SomaRecu(N-1)+N;
        return soma;
        }
    public static int Fatorial(int N){
        int fatorial = 0;
        if(N<=0) {
            return 1;
        }
        fatorial = Fatorial(N-1)*N;
        return fatorial;
    }


    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Informe um número");
        int numero = input.nextInt();
        numero=Fatorial(numero);
        System.out.println(numero);
    }
}
